[README.md](README.md)

# Taxonomies

1. [Capability](#capability)
1. [Theme](#theme)
1. [Topic](#topic)
1. [Funder](#funder)
1. [Funder Type](#funder-type)
1. [Partner](#partner)
1. [Project Status](#project-status)
1. [Resource Type](#resource-type)
1. [Staff Role](#staff-role)
1. [Position](#position)


# <a name="capability"></a>Capability

### *Fields*
- **Name**
- **Description**
- **Icon** (reference, image)

### *Terms*
- Improvement Science
- System Design
- Measure Development
- Cross-Sector Collaboration
- Patient and Family Engagement
- Health Policy


# <a name="theme"></a>Theme

### *Fields*
- **Name**
- **Description**

### *Terms*
- Quality Improvement
- Collaboration
- Innovation


# <a name="topic"></a>Topic

### *Fields*
- **Name**
- **Description**

### *Terms*
- ADHD
- Autism
- Breastfeeding
- Epilepsy
- Infant Health
- Medical Home
- Newborn Hearing Screening
- Obesity
- Patient and Family Engagement
- Sickle Cell Disease
- Special Healthcare Needs


# <a name="funder"></a>Funder

### *Fields*
- **Name**
- **Description**
- **Type** (reference, taxonomy: "Funder Type")
- **Logo** (reference, image)


# <a name="funder-type"></a>Funder Type

### *Fields*
- **Name**
- **Description**

### *Terms*
- State Agency
- Federal Agency
- Foundation
- Private


# <a name="partner"></a>Partner

### *Fields*
- **Name**
- **Description**


# <a name="project-status"></a>Project Status

### *Fields*
- **Name**
- **Description**

### *Terms*
- Active
- Completed


# <a name="resource-type"></a>Resource Type

### *Fields*
- **Name**
- **Description**

### *Terms*
- Factsheet
- Guide
- Infographic
- Podcast
- Quality Improvement Tip
- Report
- Tool or Toolkit
- Training E-Module
- Video
- Webinar Recording


# <a name="staff-role"></a>Staff Role

### *Fields*
- **Name**
- **Description**

### *Terms*
- Executive Team Member
- Board Member
- Staff Member
- Improvement Advisor


# <a name="position"></a>Position

### *Fields*
- **Name**
- **Description**

### *Terms*
- Left
- Right
- Full



