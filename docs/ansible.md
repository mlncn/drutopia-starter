# Overview

All activities, from provisioning a new droplet, to running tests, to deploying
new code may be done via Ansible.

** Please ensure your Ansible is version 2.3 or higher. Check with: **
`ansible --version`

Ansible can also be run from within the Vagrant (VirtualBox) VM. This is
recommended particularly for building/deployment (for composer/sass).
(If the VM is not configured, first run: `sudo apt-get install ansible` and
check the version)

Within the VM, to build & deploy, run:
`cd /var/www/drupal/provisioning`
`ansible-playbook -i <inventory> deploy.yml --vault-password-file=~/.vault_pass.txt`

# Running commands

The two most common scenarios are setting up a new drupal instance, or peforming
a deployment. In both cases, we use `ansible-playbook`

`ansible-playbook {options} playbook.yml`

Some common arguments for this command include:
- `-i <inventory>` specifies an inventory (list of hosts & their groupings)
- `-l <host-spec>` limit to certain hosts. May be a group, or host name, among
other things
- `--vault-password-file <file>` specifies a text file containing the vault pwd

To perform a deployment, run:

`ansible-playbook -i stage_inventory deploy.yml`

To pull a database from stage or production, use:

`ansible-playbook pull.yml --extra-vars "pull_from=@stage"`

If you wish to include the files dir, an extra-var value can be added:
`--extra-vars "pull_from=@stage include_files=true"`

# Groups

The following inventory groups were defined for this set up:
- drupal: a full drupal instance and deploy target
- dev: a development stack, useful for vagrant and CI
- ci: **Not set up yet** targets continuous integration services

# Folder Overview

### files in /provisioning

The files at this level include playbooks (ending in .yml) and inventory files.
Inventory files lists hosts and the groups they belong to, and are specified for
use by ansible-playbook with the -i option. The playbooks define sets of "plays"
(per sports analogy), which in our case mostly just defines various roles.

The roles, are, in essence, the various services that are needed, e.g. apache.
The included roles have their own playbooks, which are made up of tasks such as
copying newly built files or performing backups.

### host_vars and group_vars

Within group_vars, a file for each 'group' (as typically defined in the
inventory) defines the variable values for all members of that group.
host_vars files are similar, and may override group (and default) values, but
are named for the host name given in the inventory.

When using encrypted variables, there will exist a folder containing the
variables in vars which pull from encrypted values in vault.

### Role folders

  - defaults/main.yml (variable defaults with the lowest priority)
  - files/ (straight up files to be copied about)
  - handlers/main.yml (define handlers - typically service restarts)
  - meta/main.yml (defines dependencies of the role)
  - tasks/\*.yml - tasks (a main.yml might include other tasks in the folder)
  - templates/\* - Jinja templates

# Role overview and variable usage

The following lists all roles and the variables that are used across them.

### apache - installs and configures apache, including modules and sites.
  - ansible_nodename: automatic variable pulled from system hostname. Including
  here to note you must have the correct /etc/hostname value. If this value
  needed correction, run `/etc/init.d/hostname.sh` on the host afterwards.
  - server_aliases = A list of server aliases (may be left undefined)
  - web_dir = Possibly a bad name, as this is actually e.g. /var/www/drupal -
  i.e. do NOT include /web
### common - installs common required software, such as rsync.
  - no variables
### drupal - builds, installs, or updates drupal instances.
  - db_name - database name
  - db_user - database login user
  - db_pwd - database login password [enc]
  - db_host - defaulted to localhost
  - server_aliases - here used for trusted_host_patterns (in addition to
     ansible_nodename)
### database - installs mysql, including user and database.
  Same db_ vars as drupal: db_user, db_pwd, db_name, db_host
### php - installs and configures php and fpm
  [Defaults set are fine, for now]
  - php_fpm_svc: php7.0-fpm (service name)
  - php_fpm_path: /etc/php/7.0/fpm
  - suggested changes:
    - Either controlled by variables, or otherwise (also update apache)...
    Change to using a UNIX socket, rather than IP
    E.G., fcgi://127.0.0.1:9000 -> /run/php/php7.0-fpm.sock
    ** Files are currently treated as templates, unnecessarily.**

# Additional reading

Ansible documentation is available at https://docs.ansible.com/ansible/
There are numerous roles available in the [Ansible Galaxy](https://galaxy.ansible.com/explore#/).
Generally, these are of questionable usefulness, given they are often targeted
at one or another specific platform. However, [geerlingguy](https://github.com/geerlingguy)
has numerous [ansible roles](https://github.com/geerlingguy?utf8=%E2%9C%93&tab=repositories&q=ansible-role&type=&language=)
that are generic, but rather detailed/involved. These may be good to attempt to
integrate at some point, but for now, they are amazing references. Another very
common deployment galaxy role is "ansistrano", which is based on Capistrano v2.

# Issues

PHP 7 is not packaged for Debian 8 (aka "jessie" - which uses a 5.x release).

There are two common sources for remedying this:
- https://deb.sury.org/ - PHP 7.0 and 7.1
- https://www.dotdeb.org - PHP 7.0 only

While there is preference for using 7.1, attempts with the sury.org feed failed.

Permissions for the MySQL user are likely higher than strictly necessary.
The MySQL daemon only listens locally, but should there be any weakness in
Drupal in terms of e.g. SQL injection, this would be a potential issue.
