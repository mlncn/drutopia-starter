[README.md](README.md)

# Key Contrib Modules

* = hold off adding until needed (which will probably never be the case)

- https://www.drupal.org/project/addtoany*
- https://www.drupal.org/project/admin_toolbar
- https://www.drupal.org/project/apachesolr_paragraphs
- https://www.drupal.org/project/auto_entitylabel
- https://www.drupal.org/project/block_class
- https://www.drupal.org/project/bootstrap_library
- https://www.drupal.org/project/classy_paragraphs
- https://www.drupal.org/project/color_field
- https://www.drupal.org/project/conditional_fields
- https://www.drupal.org/project/ctools
- https://www.drupal.org/project/date
- https://www.drupal.org/project/defaultcontent
- https://www.drupal.org/project/devel
- https://www.drupal.org/project/ds*
- https://www.drupal.org/project/editor_file
- https://www.drupal.org/project/entity
- https://www.drupal.org/project/field_group
- https://www.drupal.org/project/google_analytics
- https://www.drupal.org/project/layout_plugin
- https://www.drupal.org/project/media_entity
- https://www.drupal.org/project/paragraphs
- https://www.drupal.org/project/pathauto
- https://www.drupal.org/project/smtp
- https://www.drupal.org/project/reroute_email
- https://www.drupal.org/project/rules*
- https://www.drupal.org/project/token
- https://www.drupal.org/project/twig_tweak*
- https://www.drupal.org/project/typed_data
- https://www.drupal.org/project/video_embed_field
- https://www.drupal.org/project/views_field_view
- https://www.drupal.org/project/views_slideshow
- https://www.drupal.org/project/viewsreference