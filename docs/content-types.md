[README.md](README.md)

# Content Types

1. [Page](#page)
1. [Campaign](#campaign)
1. [Project](#project)
1. [Insight](#insight)
1. [Case Study](#case-study)
1. [Action](#action)
1. [Publication](#publication)
1. [Resource](#resource)
1. [Person](#person)
1. [Video](#video)


// I've proposed using one "Resource" content type to capture all the different types of resources, given that almost all would be downloadable files (or would load in the browser separate from the theme/Drupal). Though "Insights" are considered "Resources" (at least that's where they're located in the wireframes), they're more varied and probably function more like "Pages", so I've kept them separate.

// Not sure yet how to handle E-Modules. I have them as a type of resource, which would reveal a "Link" field for entering the URL of the module, but where would that link take you? How are E-Modules featured (e.g., independent of the theme/Drupal, embedded somehow)? Also, "webinars" in the wireframes are actually the "E-Modules" resources.

// What's the best approach for handling the "Action" blocks? Should those just be blocks? If so, then can you reference a block from a paragraph? That's why I have "Action" as a content type: so that it can be rendered anywhere on a node.

// For content types that produce nodes that might be arranged as a list, I've included "Short Title" and "Short Body" fields so that the content cane be abbreviated (like a teaser).

// The "Case Study" content type includes a few fields for displaying a quote as the "card" that gets shown in a "Carousel"-type paragraph.


# <a name="page"></a>Page

### *Fields*
- **Title**
- **Body** (long text, no teaser)
- **Sections** (paragraph; "Long Text", "Content-Width Slideshow", "Quotation", "Image", "Video", "Highlight Grid", "Carousel")


# <a name="campaign"></a>Campaign

### *Fields*
- **Title**
- **Body** (long text, no teaser)
- **Sections** (paragraph; "Long Text", "Full-Width Slideshow", "Content-Width Slideshow", Quotation", "Image", "Video", "Highlight Grid", "Carousel")


# <a name="project"></a>Project

### *Fields*
- **Title**
- **Status** (reference, taxonomy: "Project Status")
- **Description** (long text, no teaser)
- **What** (long text, no teaser)
- **Who** (long text, no teaser)
- **When** (date, start and end; month and year)
- **Support** (long text, no teaser) &mdash; *this is the "Funder" label in the current project page*
- **Our Role** (long text, no teaser)
- **Funders** (entity reference, taxonomy: "Funder") &mdash; *Nothing to migrate for this one*
- **Partners** (entity reference, taxonomy: "Partner") &mdash; *Nothing to migrate for this one*


# <a name="insight"></a>Insight

### *Fields*
- **Title**
- **Short Title** (text)
- **Body** (long text, no teaser)
- **Short Body** (long text, no teaser)
- **Sections** (paragraph; "Long Text", "Content-Width Slideshow", "Quotation", "Image", "Video", "Highlight Grid", "Carousel")


# <a name="case-study"></a>Case Study

### *Fields*
- **Title**
- **Short Title** (text)
- **Body** (long text, no teaser)
- **Image** (reference, image)
- **Quote** (long text, no teaser)
- **Attribution** (text)
- **Sections** (paragraph; "Long Text", "Content-Width Slideshow", "Quotation", "Image", "Video", "Highlight Grid", "Carousel")


# <a name="action"></a>Action

### *Fields*
- **Title**
- **Body** (long text, no teaser)
- **Button** (link)


# <a name="publication"></a>Publication

### *Fields*
- **Title**
- **Authors** (text)
- **Publication** (text)
- **Date** (date, start only; month and year)
- **Abstract** (long text, no teaser)
- **Source** (boolean; "File", "Link")
- **File** (reference, file; only show if "File" selected in the "Source" field)
- **Link** (link; only show if "Link" selected in the "Source" field)


# <a name="resource"></a>Resource

### *Fields*
- **Title**
- **Body** (long text, no teaser)
- **Short Body** (long text, no teaser)
- **Featured** (boolean; "Yes", "No")
- **Type** (entity reference, taxonomy: "Resource Type")
- **Source** (boolean; "File", "Link")
- **File** (reference, file; only show if "File" selected in the "Source" field)
- **Link** (link; only show if "Link" selected in the "Source" field)


# <a name="person"></a>Person

### *Fields*
- **Title** (assigned by auto_entitylabel module)
- **First Name** (text)
- **Last Name** (text)
- **Email**
- **Credentials** (text)
- **Position/Title** (text)
- **Photo** (reference, image)
- **Role** (reference, taxonomy: "Staff Role")
- **Bio** (long text, no teaser)


# <a name="video"></a>Video

### *Fields*
- **Title** (assigned by auto_entitylabel module)
- **Subtitle** (text)
- **Video** (video)
- **Thumbnail** (reference, image)


