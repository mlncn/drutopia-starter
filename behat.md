# Behavior Driven Development with Behat

## Installation

1. Download latest version of **Selenium Standalone Server** from http://www.seleniumhq.org/download/ and place it in the root of the project.
2. Download the latest version of **ChromeDriver - WebDriver for Chrome** from https://sites.google.com/a/chromium.org/chromedriver/downloads. Extract the content of the ZIP file and place it in the root of the project.

Example:

```
.
├── Rakefile
├── behat.md
├── behat.yml
├── chromedriver
├── composer.json
├── composer.lock
├── features
├── selenium-server-standalone-3.4.0.jar
├── vendor
├── vm
└── web
```

## Running tests that do not require javascript

To run tests that **do not** have the `@javascript` annotation:

```bash
vagrant ssh
cd /vagrant
./vendor/bin/behat -c behat.yml features/

```

## Running tests that require javascript

To run tests that **have** the `@javascript` annotation:

1. Inside the virtual machine run:

```bash
vagrant ssh
cd /vagrant
java -jar /vagrant/selenium-server-standalone-3.4.0.jar -role hub

```

Replace the *selenium* version as appropriate.

2. Outside the virtual machine, from the project root, run:

```bash
cd /path/to/project/root
java -Dwebdriver.chrome.driver=./chromedriver -jar senium-server-standalone-3.4.0.jar -role node -hub http://local.nichq.org:4444/grid/register/
```

Replace the *selenium* version as appropriate. Make sure *chromedriver* is in the root of the project or update path accordingly. Make sure you have `local.nichq.org` mapped to the virtual machine's IP in `/etc/hosts` or use that use the IP directly.

3. Inside the virtual machine, with selenium running both as hub and node, run:

```bash
vagrant ssh
cd /vagrant
./vendor/bin/behat -c behat.yml features/ --tags=javascript

```