Feature: Set homepage

  @api
  @javascript
  Scenario: An administrator wants to set the homepage.
    Given I am logged in as a user with the "administrator" role
    When I go to "admin/config/system/site-information"
    And I fill in "Default front page" with "/frontpage"
    And I fill in "Default 403 (access denied) page" with ""
    And I fill in "Default 404 (not found) page" with ""
    And I press the "Save" button
    Then I should see the success message "The configuration options have been saved."
    When I go to "admin/structure/block/block-content"
    And I click "Edit" in the "Frontpage block" row
    Then I should see text matching "Node to render"
    When I fill in the autocomplete "Node to render" with "front" and click "Campaign Frontpage Demo"
    And I press the "Save" button
    Then I should see the success message "Node block Frontpage block has been updated."
    When I go to the homepage
    Then I should not see text matching "Campaign Frontpage Demo"
    And I should see text matching "This is an example campaign."