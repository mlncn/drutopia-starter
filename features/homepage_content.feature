Feature: A user sees content on the homepage

  @api
  Scenario: A user sees the terms of use and privacy policy links
    Given I go to the homepage
    Then I should see the link "Terms of Use"
    Then I should see the link "Privacy Policy"
    