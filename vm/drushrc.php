<?php

// Make sure drush can find the local site, and our aliases
$options['alias-path'] = "/var/www/drupal/drush/";
$options['r'] = '/var/www/drupal/web';
$options['l'] = 'http://local.nichq.org';

// Ensure default-context-export-references go to the right place:
$command_specific['dcer'] = array(
  'folder' => 'profiles/nichq/content',
);

// Make a manual command line si task easier:
$command_specific['site-install'] = array(
  'site-name' => 'NICHQ',
  'site-mail' => 'admin@nichq.org',
  'account-name' => 'admin',
  'account-pass' => 'admin',
);
?>
